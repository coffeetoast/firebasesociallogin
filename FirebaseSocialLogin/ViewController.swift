//
//  ViewController.swift
//  FirebaseSocialLogin
//
//  Created by SungJaeLEE on 2016. 11. 22..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import GoogleSignIn

class ViewController: UIViewController, GIDSignInUIDelegate
{
    var isLoggin: Bool = false
    
    lazy var customFBButton: UIButton = {
       let cFBB = UIButton(type: .system)
        cFBB.backgroundColor = .blue
        cFBB.setTitle("Custom FB Login", for: .normal)
        cFBB.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        cFBB.setTitleColor(.white, for: .normal)
        
        cFBB.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
        
        return cFBB
    }()
    
    func handleCustomFBLogin() {
        if !isLoggin {
            FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile","user_friends"], from: self) { (result, error) in
                guard error == nil else {
                    print("Custom FB Login failed \(error)")
                    return
                }
                // Get Access Token..............
                //print(result?.token.tokenString!)
                self.isLoggin = true
                let title = self.isLoggin ? "Logout" : "Custom FB Login"
                self.customFBButton.setTitle(title, for: .normal)
                
                self.showEmailAddress()
            }

        } else {
            self.isLoggin = false
            let title = self.isLoggin ? "Logout" : "Custom FB Login"
            self.customFBButton.setTitle(title, for: .normal)
            
            FBSDKLoginManager().logOut()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupFacebookButton()
        setupGoogleButton()

    }
    
    private func setupGoogleButton() {
        let googleButoon = GIDSignInButton()
        googleButoon.frame = CGRect(x: 16, y: 116 + 66, width: view.frame.width - 32, height: 50)
        view.addSubview(googleButoon)
        
        //Custom GoogleButton
        let customButton = UIButton(type: .system)
        customButton.frame = CGRect(x: 16, y: 116 + 66 + 66, width: view.frame.width - 32, height: 50)
        customButton.backgroundColor = .orange
        customButton.setTitleColor(.white, for: .normal)
        customButton.setTitle("Custom Google Sign In", for: .normal)
        customButton.addTarget(self, action: #selector(handleCustomGoogleSign), for: .touchUpInside)
        view.addSubview(customButton)
        
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func handleCustomGoogleSign() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    private func setupFacebookButton() {

        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        
        loginButton.frame = CGRect(x: 16, y: 50, width: view.frame.width - 32, height: 50)
        
        loginButton.delegate = self
        loginButton.readPermissions = ["email", "public_profile"]
        
        // Custom Facebook Button.............
        view.addSubview(customFBButton)
        customFBButton.frame = CGRect(x: 16, y: 116, width: view.frame.width - 32, height: 50)
    }

}

extension ViewController: FBSDKLoginButtonDelegate
{
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out of facebook")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard error == nil else {
            print(error)
            return
        }
        
        print("successfully logged in facebook........")
        showEmailAddress()
    }
    
    func showEmailAddress() {
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else {
            return
        }
        
        let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString)
        FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
            guard error == nil else {
                print("somgthing went wrong with our FBuser",error!)
                return
            }
            
            print("Successfully logged in with our user: ", user?.uid ?? "")
        })
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id, name, email"]).start { (connection, result, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            if let dic = result as? [String: AnyObject], let email = dic["email"] {
                print("email : \(email)")
            }
        }
    }

}

